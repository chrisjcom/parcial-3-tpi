/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Local;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Pais;

/**
 *
 * @author Jhossymar
 */
@Local
public interface PaisFacadeLocal {

    boolean create(Pais pais);
    
    Pais crear(Pais pais);

    boolean edit(Pais pais);
    
    Pais editar(Pais pais);

    boolean remove(Pais pais);

    Pais find(Object id);

    List<Pais> findAll();

    List<Pais> findRange(int[] range);

    int count();
    
    List<Pais> findByNombreLike(String nombre);
    
    int countByNombreLike(String nombre);
    
    List<Pais> findByIdPais(String id);
}
