/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Local;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Ruta;

/**
 *
 * @author Jhossymar
 */
@Local
public interface RutaFacadeLocal {

    boolean create(Ruta ruta);
    
    Ruta crear(Ruta ruta);

    boolean edit(Ruta ruta);
    
    Ruta editar(Ruta ruta);

    boolean remove(Ruta ruta);

    Ruta find(Object id);

    List<Ruta> findAll();

    List<Ruta> findRange(int[] range);

    int count();
    
}
