/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Local;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.TipoCliente;

/**
 *
 * @author Jhossymar
 */
@Local
public interface TipoClienteFacadeLocal {

    boolean create(TipoCliente tipoCliente);
    
    TipoCliente crear(TipoCliente tipoCliente);

    boolean edit(TipoCliente tipoCliente);
    
    TipoCliente editar(TipoCliente tipoCliente);

    boolean remove(TipoCliente tipoCliente);

    TipoCliente find(Object id);

    List<TipoCliente> findAll();

    List<TipoCliente> findRange(int[] range);

    int count();
    
}
