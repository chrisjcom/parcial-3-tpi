/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Local;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.TipoTarifa;

/**
 *
 * @author Jhossymar
 */
@Local
public interface TipoTarifaFacadeLocal {

    boolean create(TipoTarifa tipoTarifa);
    
    TipoTarifa crear(TipoTarifa tipoTarifa);

    boolean edit(TipoTarifa tipoTarifa);
    
    TipoTarifa editar(TipoTarifa tipoTarifa);

    boolean remove(TipoTarifa tipoTarifa);

    TipoTarifa find(Object id);

    List<TipoTarifa> findAll();

    List<TipoTarifa> findRange(int[] range);

    int count();
    
}
