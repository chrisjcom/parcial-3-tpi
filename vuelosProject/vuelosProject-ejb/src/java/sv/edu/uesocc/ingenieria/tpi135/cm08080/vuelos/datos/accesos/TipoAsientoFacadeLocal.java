/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Local;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.TipoAsiento;

/**
 *
 * @author Jhossymar
 */
@Local
public interface TipoAsientoFacadeLocal {

    boolean create(TipoAsiento tipoAsiento);
    
    TipoAsiento crear(TipoAsiento tipoAsiento);

    boolean edit(TipoAsiento tipoAsiento);
    
    TipoAsiento editar(TipoAsiento tipoAsiento);

    boolean remove(TipoAsiento tipoAsiento);

    TipoAsiento find(Object id);

    List<TipoAsiento> findAll();

    List<TipoAsiento> findRange(int[] range);

    int count();
    
}
