/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Pais;

/**
 *
 * @author Jhossymar
 */
@Stateless
public class PaisFacade extends AbstractFacade<Pais> implements PaisFacadeLocal, sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.PaisFacadeRemote {
    @PersistenceContext(unitName = "vuelosProject-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PaisFacade() {
        super(Pais.class);
    }
    
    public List<Pais> findByNombreLike(String nombre) {
        try {
            if(em != null) {
                Query q = em.createNamedQuery("Pais.findByNombreLike");
                q.setParameter("nombre", "%"+nombre.toUpperCase()+"%");
                return q.getResultList();
            }
        } catch(Exception e){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,e.getMessage(),e);
        }
        return null;
    }
    
    public int countByNombreLike(String nombre) {
       try {
            if(em != null) {
                Query q = em.createNamedQuery("Pais.countByNombreLike");
                q.setParameter("nombre", "%"+nombre.toUpperCase()+"%");
                return ((Long) q.getSingleResult()).intValue();
            }
        } catch(Exception e){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,e.getMessage(),e);
        }
        return 0;
    }
    
    public List<Pais> findByIdPais(String id) {
        try {
            if(em != null) {
                Query q = em.createNamedQuery("Pais.findByIdPais");
                q.setParameter("idPais",id);
                return q.getResultList();
            }
        } catch(Exception e){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,e.getMessage(),e);
        }
        return null;
    }
    
}
