/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.MedioContacto;

/**
 *
 * @author Jhossymar
 */
@Stateless
public class MedioContactoFacade extends AbstractFacade<MedioContacto> implements MedioContactoFacadeLocal, sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.MedioContactoFacadeRemote {
    @PersistenceContext(unitName = "vuelosProject-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MedioContactoFacade() {
        super(MedioContacto.class);
    }
    
}
