/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Local;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.DetalleAvion;

/**
 *
 * @author Jhossymar
 */
@Local
public interface DetalleAvionFacadeLocal {

    boolean create(DetalleAvion detalleAvion);
    
    DetalleAvion crear(DetalleAvion detalleAvion);

    boolean edit(DetalleAvion detalleAvion);
    
    DetalleAvion editar(DetalleAvion detalleAvion);

    boolean remove(DetalleAvion detalleAvion);

    DetalleAvion find(Object id);

    List<DetalleAvion> findAll();

    List<DetalleAvion> findRange(int[] range);

    int count();
    
}
