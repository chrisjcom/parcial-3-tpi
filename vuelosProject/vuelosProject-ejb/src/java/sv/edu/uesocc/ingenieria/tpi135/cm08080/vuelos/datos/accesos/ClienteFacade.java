/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Cliente;

/**
 *
 * @author Jhossymar
 */
@Stateless
public class ClienteFacade extends AbstractFacade<Cliente> implements ClienteFacadeLocal, sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.ClienteFacadeRemote {
    @PersistenceContext(unitName = "vuelosProject-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClienteFacade() {
        super(Cliente.class);
    }
    
    public List<Cliente> findByApellidosLike(String apellidos) {
        try {
            if(em != null) {
                Query q = em.createNamedQuery("Cliente.findByApellidosLike");
                q.setParameter("apellidos", "%"+apellidos.toUpperCase()+"%");
                return q.getResultList();
            }
        } catch(Exception e){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,e.getMessage(),e);
        }
        return null;
    }
    
    public int countByApellidosLike(String apellidos) {
       try {
            if(em != null) {
                Query q = em.createNamedQuery("Cliente.countByApellidosLike");
                q.setParameter("apellidos", "%"+apellidos.toUpperCase()+"%");
                return ((Long) q.getSingleResult()).intValue();
            }
        } catch(Exception e){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,e.getMessage(),e);
        }
        return 0;
    }
}
