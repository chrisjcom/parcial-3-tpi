/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Local;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.TipoVuelo;

/**
 *
 * @author Jhossymar
 */
@Local
public interface TipoVueloFacadeLocal {

    boolean create(TipoVuelo tipoVuelo);
    
    TipoVuelo crear(TipoVuelo tipoVuelo);

    boolean edit(TipoVuelo tipoVuelo);
    
    TipoVuelo editar(TipoVuelo tipoVuelo);

    boolean remove(TipoVuelo tipoVuelo);

    TipoVuelo find(Object id);

    List<TipoVuelo> findAll();

    List<TipoVuelo> findRange(int[] range);

    int count();
    
}
