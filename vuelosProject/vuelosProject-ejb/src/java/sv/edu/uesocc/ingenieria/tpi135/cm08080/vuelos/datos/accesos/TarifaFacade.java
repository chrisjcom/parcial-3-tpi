/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Tarifa;

/**
 *
 * @author Jhossymar
 */
@Stateless
public class TarifaFacade extends AbstractFacade<Tarifa> implements TarifaFacadeLocal, sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.TarifaFacadeRemote {
    @PersistenceContext(unitName = "vuelosProject-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TarifaFacade() {
        super(Tarifa.class);
    }
    
}
