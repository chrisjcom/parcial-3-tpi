/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Boleto;

/**
 *
 * @author Jhossymar
 */
@Stateless
public class BoletoFacade extends AbstractFacade<Boleto> implements BoletoFacadeLocal, sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.BoletoFacadeRemote {
    @PersistenceContext(unitName = "vuelosProject-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BoletoFacade() {
        super(Boleto.class);
    }
    
}
