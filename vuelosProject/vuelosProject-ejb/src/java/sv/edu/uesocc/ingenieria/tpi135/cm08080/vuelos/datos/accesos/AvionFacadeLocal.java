/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Local;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Avion;

/**
 *
 * @author Jhossymar
 */
@Local
public interface AvionFacadeLocal {

    boolean create(Avion avion);
    
    Avion crear(Avion avion);

    boolean edit(Avion avion);
    
    Avion editar(Avion avion);

    boolean remove(Avion avion);

    Avion find(Object id);

    List<Avion> findAll();

    List<Avion> findRange(int[] range);

    int count();
    
}
