/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Local;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.TipoNodo;

/**
 *
 * @author Jhossymar
 */
@Local
public interface TipoNodoFacadeLocal {

    boolean create(TipoNodo tipoNodo);
    
    TipoNodo crear(TipoNodo tipoNodo);

    boolean edit(TipoNodo tipoNodo);
    
    TipoNodo editar(TipoNodo tipoNodo);

    boolean remove(TipoNodo tipoNodo);

    TipoNodo find(Object id);

    List<TipoNodo> findAll();

    List<TipoNodo> findRange(int[] range);

    int count();
    
}
