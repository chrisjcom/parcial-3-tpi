/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Local;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Cliente;

/**
 *
 * @author Jhossymar
 */
@Local
public interface ClienteFacadeLocal {

    boolean create(Cliente cliente);
    
    Cliente crear(Cliente cliente);

    boolean edit(Cliente cliente);
    
    Cliente editar(Cliente cliente);

    boolean remove(Cliente cliente);

    Cliente find(Object id);

    List<Cliente> findAll();

    List<Cliente> findRange(int[] range);

    int count();
    
    List<Cliente> findByApellidosLike(String apellidos);
    
    int countByApellidosLike(String apellidos);
    
}
