/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.ws;

import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.jboss.weld.util.collections.ArraySet;

/**
 *
 * @author Jhossymar
 */

@ApplicationPath("ws")
public class AppConfig extends Application{
    
    @Override
    public Set<Class<?>> getClasses() {
        return algo();
    }
    
    public Set algo(){
        Set<Class> algo = new ArraySet<>();
        algo.add(TipoAsientoResource.class);
        algo.add(PaisResource.class);
        algo.add(TipoClienteResource.class);
        return algo;
    }
}
