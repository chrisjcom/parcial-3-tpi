/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.backing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.ClienteFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.PaisFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Cliente;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Pais;

/**
 *
 * @author Jhossymar
 */
@ManagedBean
@ViewScoped
public class FrmPais implements Serializable{
    @EJB
    private PaisFacadeLocal pfl;
    @EJB
    private ClienteFacadeLocal cfl;
    private Pais registro;
    private LazyDataModel<Pais> modelo;
    private boolean editar = false;
    private boolean agregar = false;
    private LazyDataModel<Cliente> modeloCliente;
    
    @PostConstruct
    private void iniciar(){
        this.setModelo(new LazyDataModel<Pais>(){

            @Override
            public List<Pais> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                List salida = new ArrayList();
                try {
                    if(filters != null) {
                        for (Iterator<Map.Entry<String, Object>> it = filters.entrySet().iterator(); it.hasNext();) {
                            Map.Entry<String, Object> entry = it.next();
                            System.out.println(entry.getKey());
                            if(entry.getKey().equals("nombre")){    
                             if(pfl != null) {
                                 this.setRowCount(pfl.countByNombreLike(entry.getValue().toString()));
                                 salida = pfl.findByNombreLike(entry.getValue().toString());
                                 return salida;
                             }   
                            }
                        }
                    }
                    if(pfl != null){
                        this.setRowCount(pfl.count());
                        int[] rango = new int[2];
                        rango[0] = first;
                        rango[1] = pageSize;
                        salida = pfl.findRange(rango);
                    }
                    return salida;
                } catch (Exception e) {
                }
                return salida;
            }

            @Override
            public Object getRowKey(Pais object) {
                return object.getIdPais();
            }

            @Override
            public Pais getRowData(String rowKey) {
                if(this.getWrappedData()!=null){
                    List<Pais> lista = (List<Pais>) this.getWrappedData();
                    if(!lista.isEmpty()) {
                        for(Pais get : lista) {
                            if(get.getIdPais().compareToIgnoreCase(rowKey)==0) {
                                return get;
                            }
                        }
                    }
                }
                return null;
            }
        });
        
        this.setModeloCliente(new LazyDataModel<Cliente>() {    

            @Override
            public List<Cliente> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                List salida = new ArrayList();
                if(registro != null){
                    Pais pais = registro;
                    List<Cliente> lista;
                    if(cfl != null) {
                        lista = cfl.findAll();
                        if(pais != null && !lista.isEmpty()) {            
                            for(Cliente get : lista) {
                                if(get.getClientePK().getIdPais().compareToIgnoreCase(pais.getIdPais())==0) {
                                    salida.add(get); 
                                }
                            }
                        }
                    }
                    this.setRowCount(salida.size());
                    return salida;
                } else {
                    if(cfl != null) {
                        this.setRowCount(cfl.count());
                        int[] rango = new int[2];
                        rango[0] = first;
                        rango[1] = pageSize;
                        salida = cfl.findRange(rango);
                        return salida;
                    } else {
                        return salida;
                    }
                }
            }
            
        });
    }

    /**
     * @return the registro
     */
    public Pais getRegistro() {
        return registro;
    }

    /**
     * @param registro the registro to set
     */
    public void setRegistro(Pais registro) {
        this.registro = registro;
    }

    /**
     * @return the modelo
     */
    public LazyDataModel<Pais> getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(LazyDataModel modelo) {
        this.modelo = modelo;
    }
    
    public void btnModificarAction(ActionEvent ae){
        try{
            boolean resultado = this.pfl.edit(registro);
            FacesMessage msj = new FacesMessage(FacesMessage.SEVERITY_INFO, resultado?"Modificado con exito":"Error", null);
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, msj);
        }catch(Exception e){
            
        }
    }
    
    public void btnNuevoAction(ActionEvent ae) {
        this.editar = false;
        try{
            this.registro = new Pais();
        }catch(Exception e){
            
        }
    }
    
    public void btnGuardarAction(ActionEvent ae){
        try {
            if(this.registro != null && this.pfl != null){
                boolean resultado = this.pfl.create(registro);
                FacesMessage msj = new FacesMessage(FacesMessage.SEVERITY_INFO, resultado?"Creado con exito":"Error", null);
                this.agregar = !resultado;
                FacesContext.getCurrentInstance().addMessage(null, msj);
            }
        } catch (Exception e) {
        }
    }
    
    public void btnEliminarAction(ActionEvent ae) {
        try {
            if(this.registro != null && this.pfl != null){
                boolean resultado = this.pfl.remove(registro);
                FacesMessage msj = new FacesMessage(FacesMessage.SEVERITY_INFO, resultado?"Eliminado con exito":"Error", null);
                FacesContext.getCurrentInstance().addMessage(null, msj);
            }
        } catch (Exception e) {
        }
    }

    /**
     * @return the editar
     */
    public boolean isEditar() {
        return editar;
    }

    /**
     * @param editar the editar to set
     */
    public void setEditar(boolean editar) {
        this.editar = editar;
    }
    
    public void cambioTabla(){
        this.editar = true;
    }

    /**
     * @return the agregar
     */
    public boolean isAgregar() {
        return agregar;
    }

    /**
     * @param agregar the agregar to set
     */
    public void setAgregar(boolean agregar) {
        this.agregar = agregar;
    }

    /**
     * @return the modeloCliente
     */
    public LazyDataModel<Cliente> getModeloCliente() {
        return modeloCliente;
    }

    /**
     * @param modeloCliente the modeloCliente to set
     */
    public void setModeloCliente(LazyDataModel<Cliente> modeloCliente) {
        this.modeloCliente = modeloCliente;        
    }
 
}

