/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.backing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.ClienteFacadeRemote;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.PaisFacadeRemote;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.TipoClienteFacadeRemote;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Cliente;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.ClientePK;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Pais;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.TipoCliente;

/**
 *
 * @author Jhossymar
 */
@ManagedBean
@ViewScoped
public class FrmClientes implements Serializable{
   @EJB
   private ClienteFacadeRemote cfl;
   @EJB
   private PaisFacadeRemote pfl;
   @EJB
   private TipoClienteFacadeRemote tcfl;
   private LazyDataModel<Cliente> modeloCliente;
   private Cliente registro;
   private List<Pais> listaPaises;
   private List<TipoCliente> listaTipoCliente;
   private ClientePK cpk;
   private boolean renderDetalles = false;
   private boolean cancelar = false;
   private boolean editar = false;
   private boolean crear = false;
   private boolean nuevo = true;
   
   @PostConstruct
   private void iniciar(){
       
       this.listaPaises = pfl.findAll();
       
       this.listaTipoCliente = tcfl.findAll();
       
       this.setModeloCliente(new LazyDataModel<Cliente>() {    

            @Override
            public List<Cliente> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                List salida = new ArrayList();
                if(filters != null) {
                        for (Iterator<Map.Entry<String, Object>> it = filters.entrySet().iterator(); it.hasNext();) {
                            Map.Entry<String, Object> entry = it.next();
                            System.out.println(entry.getKey());
                            if(entry.getKey().equals("apellidos")){    
                             if(cfl != null) {
                                 this.setRowCount(cfl.countByApellidosLike(entry.getValue().toString()));
                                 salida = cfl.findByApellidosLike(entry.getValue().toString());
                                 return salida;
                             }   
                            }
                        }
                    }
                
                if(cfl != null){
                    this.setRowCount(cfl.count());
                    int[] rango = new int[2];
                    rango[0] = first;
                    rango[1] = pageSize;
                    salida = cfl.findRange(rango);
                }
                return salida;
            }
            
            @Override
            public Object getRowKey(Cliente object) {
                if(object != null) {
                    return object.getClientePK();
                }
                return null;
            }
            
            @Override
            public Cliente getRowData(String rowKey) {
                if(this.getWrappedData()!=null && rowKey != null){
                    List<Cliente> lista = (List<Cliente>) this.getWrappedData();
                    if(!lista.isEmpty()) {
                        for(Cliente get : lista) {
                            if(get.getClientePK().toString().compareToIgnoreCase(rowKey)==0) {
                                return get;
                            }
                        }
                    }
                }
                return null;
            }
            
        });
   }
   
   public void btnNuevoAction(ActionEvent ae) {
       this.nuevo =  false;
       this.cancelar = true;
       this.crear = true;
       this.registro = new Cliente();
       this.cpk = new ClientePK();
       this.cpk.setPasaporte("");
       this.cpk.setIdPais("");
       this.registro.setClientePK(this.cpk);
       this.renderDetalles = true;
   }
   
   public void btnCancelarAction(ActionEvent ae) {
       this.nuevo =  true;
       this.cancelar = false;
       this.crear = false;
       this.editar = false;
       this.registro = null;
       this.renderDetalles = false;
   }
   
   public void btnCrearAction(ActionEvent ae){
       try {
           if(this.registro != null && this.cfl != null) {
               boolean resultado = this.cfl.create(registro);
               FacesMessage msj = new FacesMessage(FacesMessage.SEVERITY_INFO, resultado?"Creado con exito":"Error", null);
               this.crear = !resultado;
               this.cancelar = !resultado;
               this.nuevo = resultado;
               this.renderDetalles = !resultado;
               FacesContext.getCurrentInstance().addMessage(null, msj);
           }
       } catch (Exception e) {
       }
   }
   
   public void btnModificarAction(ActionEvent ae) {
       try {
           if(this.registro != null && this.cfl != null) {
               boolean resultado = this.cfl.edit(registro);
               FacesMessage msj = new FacesMessage(FacesMessage.SEVERITY_INFO, resultado?"Editado con exito":"Error", null);
               this.editar = !resultado;
               this.cancelar = !resultado;
               this.nuevo = resultado;
               this.renderDetalles = !resultado;
               FacesContext.getCurrentInstance().addMessage(null, msj);
           }
       } catch (Exception e) {
       }
   }
   
   public void btnEliminarAction(ActionEvent ae){
       try {
           if(this.registro != null && this.cfl != null) {
               boolean resultado = this.cfl.remove(registro);
               FacesMessage msj = new FacesMessage(FacesMessage.SEVERITY_INFO, resultado?"Eliminado con exito":"Error", null);
               this.editar = !resultado;
               this.cancelar = !resultado;
               this.nuevo = resultado;
               this.renderDetalles = !resultado;
               FacesContext.getCurrentInstance().addMessage(null, msj);
           }
       } catch (Exception e) {
       }
   }

    /**
     * @return the modeloCliente
     */
    public LazyDataModel<Cliente> getModeloCliente() {
        return modeloCliente;
    }

    /**
     * @param modeloCliente the modeloCliente to set
     */
    public void setModeloCliente(LazyDataModel<Cliente> modeloCliente) {
        this.modeloCliente = modeloCliente;
    }

    /**
     * @return the registro
     */
    public Cliente getRegistro() {
        return registro;
    }

    /**
     * @param registro the registro to set
     */
    public void setRegistro(Cliente registro) {
        this.registro = registro;
    }

    /**
     * @return the listaPaises
     */
    public List<Pais> getListaPaises() {
        return listaPaises;
    }    
    
    public String getPaisSeleccionado(){
        if(registro != null) {
            if(registro.getClientePK() != null) {
                return registro.getClientePK().getIdPais();
            } else {
              return null;   
            }
        }else {
            return null;
        }
    }
    
    public void setPaisSeleccionado(String id){
        if(id != null && !this.listaPaises.isEmpty()){
            for(Pais p : this.listaPaises) {
                if(p.getIdPais().compareToIgnoreCase(id) == 0) {
                    if(this.registro.getClientePK() != null){
                        this.registro.getClientePK().setIdPais(id);
                        this.registro.setPais(p);
                    } else {
                        this.registro.setPais(p);
                        this.registro.getClientePK().setIdPais(id);
                    }
                }
            }
        }
    }
    
    public Integer getTipoClienteSeleccionado(){
        if(registro != null){
            if(registro.getIdTipoCliente() != null){
                return registro.getIdTipoCliente().getIdTipoCliente();
            } else {
                return null;
            }         
        } else {
            return null;
        }
    }
    
    public void setTipoClienteSeleccionado(Integer id){
        if(id >= 0 && !this.listaTipoCliente.isEmpty()){
            for(TipoCliente tc : this.getListaTipoCliente()) {
                if(Objects.equals(tc.getIdTipoCliente(), id)) {
                    if(this.registro.getIdTipoCliente() != null) {
                        this.registro.getIdTipoCliente().setIdTipoCliente(id);
                    } else {
                        this.registro.setIdTipoCliente(tc);
                    }
                }
            }
        }
    }

    /**
     * @return the renderDetalles
     */
    public boolean isRenderDetalles() {
        return renderDetalles;
    }

    /**
     * @param renderDetalles the renderDetalles to set
     */
    public void setRenderDetalles(boolean renderDetalles) {
        this.renderDetalles = renderDetalles;
    }
    
    public void mostrarDetalles(){
        this.renderDetalles = true;
        this.editar =  true;
        this.cancelar =  true;
        this.nuevo = false;
    }

    /**
     * @return the listaTipoCliente
     */
    public List<TipoCliente> getListaTipoCliente() {
        return listaTipoCliente;
    }

    /**
     * @return the cancelar
     */
    public boolean isCancelar() {
        return cancelar;
    }

    /**
     * @param cancelar the cancelar to set
     */
    public void setCancelar(boolean cancelar) {
        this.cancelar = cancelar;
    }

    /**
     * @return the editar
     */
    public boolean isEditar() {
        return editar;
    }

    /**
     * @param editar the editar to set
     */
    public void setEditar(boolean editar) {
        this.editar = editar;
    }

    /**
     * @return the crear
     */
    public boolean isCrear() {
        return crear;
    }

    /**
     * @param crear the crear to set
     */
    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    /**
     * @return the nuevo
     */
    public boolean isNuevo() {
        return nuevo;
    }

    /**
     * @param nuevo the nuevo to set
     */
    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }
}
