/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.websocket;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Pais;

/**
 *
 * @author Jhossymar
 */
@ServerEndpoint(value="/PaisWsocket")
public class PaisWsocket {
    private static Set<Session> clientes = Collections.synchronizedSet(new HashSet<Session>());
    
    @OnMessage
    public void recibirMensaje(String mensaje){
        try {
            if(clientes!=null && !clientes.isEmpty()){
            for(Session s : clientes){
                s.getBasicRemote().sendText(mensaje);
            }
        }
        } catch (Exception e) {
        }
    }
    
    @OnOpen
    public void onOpen(Session s){
        clientes.add(s);
    }
    
    @OnClose
    public void onClose(Session s){
        clientes.remove(s);
    }
}
