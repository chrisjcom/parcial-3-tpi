/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.ws.SOAP;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.TipoAsientoFacadeRemote;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.TipoAsiento;

/**
 *
 * @author Jhossymar
 */

@WebService(serviceName = "vuelosProject-war", name = "SoapWSTipoAsiento")
@Stateless
public class TipoAsientoWS {
    @EJB
    private TipoAsientoFacadeRemote tafr;
    
   
    @WebMethod(operationName = "findAll")
    public List<TipoAsiento> findAll(){
        List salida = new ArrayList();
        try {
            if(tafr != null) {
                salida = tafr.findAll();
            }
        } catch (Exception e) {
        }
        return salida;
        
    }  
}
