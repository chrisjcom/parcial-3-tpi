/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.ws;

import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.TipoAsientoFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.TipoAsiento;

/**
 *
 * @author Jhossymar
 */
@RequestScoped
@Path("TipoAsiento")
public class TipoAsientoResource {
    @Inject
    private TipoAsientoFacadeLocal tafl;
    
    @GET
    public List<TipoAsiento> findAll(){
        List salida = new ArrayList();
        try {
            if(tafl != null) {
                salida = tafl.findAll();
            }
        } catch (Exception e) {
        }
        return salida;
    }
    
}
