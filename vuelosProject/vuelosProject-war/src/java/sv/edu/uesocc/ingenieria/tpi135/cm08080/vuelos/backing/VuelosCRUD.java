/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.backing;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.AvionFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.BoletoFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.ClienteFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.DetalleAvionFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.DetalleBoletoFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.DetalleVueloFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.MedioContactoFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.NodoFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.PaisFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.RutaFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.TarifaFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.TipoAsientoFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.TipoClienteFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.TipoMedioContactoFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.TipoNodoFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.TipoTarifaFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.TipoVueloFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.VueloFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Avion;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Boleto;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Cliente;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.DetalleAvion;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.DetalleBoleto;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.DetalleVuelo;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.MedioContacto;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Nodo;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Pais;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Ruta;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Tarifa;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.TipoAsiento;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.TipoCliente;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.TipoMedioContacto;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.TipoNodo;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.TipoTarifa;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.TipoVuelo;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Vuelo;

/**
 *
 * @author Jhossymar
 */
@ManagedBean
@ViewScoped
public class VuelosCRUD implements Serializable{
    private Avion registroAvion;
    private Boleto registroBoleto;
    private Cliente registroCliente;
    private DetalleAvion registroDetalleAvion;
    private DetalleBoleto registroDetalleBoleto;
    private DetalleVuelo registroDetalleVuelo;
    private MedioContacto registroMedioContacto;
    private Nodo registroNodo;
    private Pais registroPais;
    private Ruta registroRuta;
    private Tarifa registroTarifa;
    private TipoAsiento registroTipoAsiento;
    private TipoCliente registroTipoCliente;
    private TipoMedioContacto registroTipoMedioContacto;
    private TipoNodo registroTipoNodo;
    private TipoTarifa registroTipoTarifa;
    private TipoVuelo registroTipoVuelo;
    private Vuelo registroVuelo;
    private boolean editar = false;
    private boolean agregar = false;
    private boolean resultado = false;
        
    @EJB
    private AvionFacadeLocal avionFL;
    @EJB
    private BoletoFacadeLocal boletoFL;
    @EJB
    private ClienteFacadeLocal clienteFL;
    @EJB
    private DetalleAvionFacadeLocal detalleAvionFL;
    @EJB
    private DetalleBoletoFacadeLocal detalleBoletoFL;
    @EJB
    private DetalleVueloFacadeLocal detalleVueloFL;
    @EJB
    private MedioContactoFacadeLocal medioContactoFL;
    @EJB
    private NodoFacadeLocal nodoFL;
    @EJB
    private PaisFacadeLocal paisFL;
    @EJB
    private RutaFacadeLocal rutaFL;
    @EJB
    private TarifaFacadeLocal tarifaFL;
    @EJB
    private TipoAsientoFacadeLocal tipoAsientoFL;
    @EJB
    private TipoClienteFacadeLocal tipoClienteFL;
    @EJB
    private TipoMedioContactoFacadeLocal tipoMedioContactoFL;
    @EJB
    private TipoNodoFacadeLocal tipoNodoFL;
    @EJB
    private TipoTarifaFacadeLocal tipoTarifaFL;
    @EJB
    private TipoVueloFacadeLocal tipoVueloFL;
    @EJB
    private VueloFacadeLocal vueloFL;
    
    public void modificarAvion(ActionEvent ae) {
        try {
            if(registroAvion != null && avionFL != null){
                resultado = this.avionFL.edit(registroAvion);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    }
    
    public void modificarBoleto(ActionEvent ae) {
        try {
            if(registroBoleto != null && boletoFL != null){
                resultado = this.boletoFL.edit(registroBoleto);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    }
    
    public void modificarCliente(ActionEvent ae) {
        try {
            if(registroCliente != null && clienteFL != null){
                resultado = this.clienteFL.edit(registroCliente);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    }
    
    public void modificarDetalleAvion(ActionEvent ae) {
        try {
            if(registroDetalleAvion != null && detalleAvionFL != null){
                resultado = this.detalleAvionFL.edit(registroDetalleAvion);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    }
    
    public void modificarDetalleBoleto(ActionEvent ae) {
        try {
            if(registroDetalleBoleto != null && detalleBoletoFL != null){
                resultado = this.detalleBoletoFL.edit(registroDetalleBoleto);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    }
    
    public void modificarDetalleVuelo(ActionEvent ae) {
        try {
            if(registroDetalleVuelo != null && detalleVueloFL != null){
                resultado = this.detalleVueloFL.edit(registroDetalleVuelo);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    }
    
    public void modificarMedioContacto(ActionEvent ae) {
        try {
            if(registroMedioContacto != null && medioContactoFL != null){
                resultado = this.medioContactoFL.edit(registroMedioContacto);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    }
    
    public void modificarNodo(ActionEvent ae) {
        try {
            if(registroNodo != null && nodoFL != null){
                resultado = this.nodoFL.edit(registroNodo);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    }
    
    public void modificarPais(ActionEvent ae) {
        try {
            if(registroRuta != null && rutaFL != null){
                resultado = this.rutaFL.edit(registroRuta);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    }
    
    public void modificarRuta(ActionEvent ae) {
        try {
            if(registroRuta != null && rutaFL != null){
                resultado = this.rutaFL.edit(registroRuta);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    }
    
    public void modificarTarifa(ActionEvent ae) {
        try {
            if(registroTarifa != null && tarifaFL != null){
                resultado = this.tarifaFL.edit(registroTarifa);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    }
    
    public void modificarTipoAsiento(ActionEvent ae) {
        try {
            if(registroTipoAsiento != null && tipoAsientoFL != null){
                resultado = this.tipoAsientoFL.edit(registroTipoAsiento);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    }
    
    public void modificarTipoCliente(ActionEvent ae) {
        try {
            if(registroTipoCliente != null && tipoClienteFL != null){
                resultado = this.tipoClienteFL.edit(registroTipoCliente);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    }
    
    public void modificarTipoMedioContacto(ActionEvent ae) {
        try {
            if(registroTipoMedioContacto != null && tipoMedioContactoFL != null){
                resultado = this.tipoMedioContactoFL.edit(registroTipoMedioContacto);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    }
    
    public void modificarTipoNodo(ActionEvent ae) {
        try {
            if(registroTipoNodo != null && tipoNodoFL != null){
                resultado = this.tipoNodoFL.edit(registroTipoNodo);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    } 
    
    public void modificarTipotarifa(ActionEvent ae) {
        try {
            if(registroTipoTarifa != null && tipoTarifaFL != null){
                resultado = this.tipoTarifaFL.edit(registroTipoTarifa);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    } 
    
    public void modificarTipoVuelo(ActionEvent ae) {
        try {
            if(registroTipoVuelo != null && tipoVueloFL != null){
                resultado = this.tipoVueloFL.edit(registroTipoVuelo);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    } 
    
    public void modificarVuelo(ActionEvent ae) {
        try {
            if(registroVuelo != null && vueloFL != null){
                resultado = this.vueloFL.edit(registroVuelo);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
        }
    }
    
    public void nuevoAvion(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroAvion = new Avion();
        } catch (Exception e) {
        }
    }
    
    public void nuevoBoleto(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroBoleto = new Boleto();
        } catch (Exception e) {
        }
    }
    
    public void nuevoCliente(ActionEvent ae) {
       try {
            this.editar = false;
            this.registroCliente = new Cliente();
        } catch (Exception e) {
        }
    }
    
    public void nuevoDetalleAvion(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroDetalleAvion = new DetalleAvion();
        } catch (Exception e) {
        }
    }
    
    public void nuevoDetalleBoleto(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroDetalleBoleto = new DetalleBoleto();
        } catch (Exception e) {
        }
    }
    
    public void nuevoDetalleVuelo(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroDetalleVuelo = new DetalleVuelo();
        } catch (Exception e) {
        }
    }
    
    public void nuevoMedioContacto(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroMedioContacto = new MedioContacto();
        } catch (Exception e) {
        }
    }
    
    public void nuevoNodo(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroNodo = new Nodo();
        } catch (Exception e) {
        }
    }
    
    public void nuevoPais(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroPais = new Pais();
        } catch (Exception e) {
        }
    }
    
    public void nuevoRuta(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroRuta = new Ruta();
        } catch (Exception e) {
        }
    }
    
    public void nuevoTarifa(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroTarifa = new Tarifa();
        } catch (Exception e) {
        }
    }
    
    public void nuevoTipoAsiento(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroTipoAsiento = new TipoAsiento();
        } catch (Exception e) {
        }
    }
    
    public void nuevoTipoCliente(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroTipoCliente = new TipoCliente();
        } catch (Exception e) {
        }
    }
    
    public void nuevoTipoMedioContacto(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroTipoMedioContacto = new TipoMedioContacto();
        } catch (Exception e) {
        }
    }
    
    public void nuevoTipoNodo(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroTipoNodo = new TipoNodo();
        } catch (Exception e) {
        }
    } 
    
    public void nuevoTipotarifa(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroTipoTarifa = new TipoTarifa();
        } catch (Exception e) {
        }
    } 
    
    public void nuevoTipoVuelo(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroTipoVuelo = new TipoVuelo();
        } catch (Exception e) {
        }
    } 
    
    public void nuevoVuelo(ActionEvent ae) {
        try {
            this.editar = false;
            this.registroVuelo = new Vuelo();
        } catch (Exception e) {
        }
    }
    
    public void guardarAvion(ActionEvent ae){
        try {
            if(this.registroAvion != null && this.avionFL != null){
                resultado = this.avionFL.create(registroAvion);
                this.agregar = !resultado;
                FacesContext.getCurrentInstance().addMessage(null, createMsj("Creado con éxito",resultado));
            }
        } catch (Exception e) {
        }
    }
    
    public void guardarBoleto(ActionEvent ae){
        try {
            if(this.registroBoleto != null && this.boletoFL != null){
                resultado = this.boletoFL.create(registroBoleto);
                this.agregar = !resultado;
                FacesContext.getCurrentInstance().addMessage(null, createMsj("Creado con éxito",resultado));
            }
        } catch (Exception e) {
        }
    }
    
    public void guardarCliente(ActionEvent ae){
        try {
            if(this.registroCliente != null && this.clienteFL != null){
                resultado = this.clienteFL.create(registroCliente);
                this.agregar = !resultado;
                FacesContext.getCurrentInstance().addMessage(null, createMsj("Creado con éxito",resultado));
            }
        } catch (Exception e) {
        }
    }
    
    public void guardarDetalleAvion(ActionEvent ae){
        try {
            if(this.registroDetalleAvion != null && this.detalleAvionFL != null){
                resultado = this.detalleAvionFL.create(registroDetalleAvion);
                this.agregar = !resultado;
                FacesContext.getCurrentInstance().addMessage(null, createMsj("Creado con éxito",resultado));
            }
        } catch (Exception e) {
        }
    }
    
    public void guardarDetalleBoleto(ActionEvent ae){
        try {
            if(this.registroDetalleBoleto != null && this.detalleBoletoFL != null){
                resultado = this.detalleBoletoFL.create(registroDetalleBoleto);
                this.agregar = !resultado;
                FacesContext.getCurrentInstance().addMessage(null, createMsj("Creado con éxito",resultado));
            }
        } catch (Exception e) {
        }
    }
    
/*    public void modificarAction(ActionEvent ae) {        
        try {
            if(registroAvion != null && avionFL != null){
                resultado = this.avionFL.edit(registroAvion);
            } else if(registroBoleto != null && boletoFL != null){
                resultado = this.boletoFL.edit(registroBoleto);
            } else if(registroCliente != null && clienteFL != null){
                resultado = this.clienteFL.edit(registroCliente);
            } else if(registroDetalleAvion != null && detalleAvionFL != null){
                resultado = this.detalleAvionFL.edit(registroDetalleAvion);
            } else if(registroDetalleBoleto != null && detalleBoletoFL != null){
                resultado = this.detalleBoletoFL.edit(registroDetalleBoleto);
            } else if(registroDetalleVuelo != null && detalleVueloFL != null){
                resultado = this.detalleVueloFL.edit(registroDetalleVuelo);
            } else if(registroMedioContacto != null && medioContactoFL != null){
                resultado = this.medioContactoFL.edit(registroMedioContacto);
            } else if(registroNodo != null && nodoFL != null){
                resultado = this.nodoFL.edit(registroNodo);
            } else if(registroPais != null && paisFL != null){
                resultado = this.paisFL.edit(registroPais);
            } else if(registroRuta != null && rutaFL != null){
                resultado = this.rutaFL.edit(registroRuta);
            } else if(registroTarifa != null && tarifaFL != null){
                resultado = this.tarifaFL.edit(registroTarifa);
            } else if(registroTipoAsiento != null && tipoAsientoFL != null){
                resultado = this.tipoAsientoFL.edit(registroTipoAsiento);
            } else if(registroTipoCliente != null && tipoClienteFL != null){
                resultado = this.tipoClienteFL.edit(registroTipoCliente);
            } else if(registroTipoMedioContacto != null && tipoMedioContactoFL != null){
                resultado = this.tipoMedioContactoFL.edit(registroTipoMedioContacto);
            } else if(registroTipoNodo != null && tipoNodoFL != null){
                resultado = this.tipoNodoFL.edit(registroTipoNodo);
            } else if(registroTipoTarifa != null && tipoTarifaFL != null){
                resultado = this.tipoTarifaFL.edit(registroTipoTarifa);
            } else if(registroTipoVuelo != null && tipoVueloFL != null){
                resultado = this.tipoVueloFL.edit(registroTipoVuelo);
            } else if(registroVuelo != null && vueloFL != null){
                resultado = this.vueloFL.edit(registroVuelo);
            }
            this.editar = resultado;
            FacesContext.getCurrentInstance().addMessage(null, createMsj("Modificado con éxito", resultado));
        } catch (Exception e) {
            
        }
    }*/
    
    public FacesMessage createMsj(String texto, boolean resultado) {
        FacesMessage msj = new FacesMessage(FacesMessage.SEVERITY_INFO, resultado?texto:"Error", null);
        return msj;
    }
    
    /**
     * @return the registroAvion
     */
    public Avion getRegistroAvion() {
        return registroAvion;
    }

    /**
     * @param registroAvion the registroAvion to set
     */
    public void setRegistroAvion(Avion registroAvion) {
        this.registroAvion = registroAvion;
    }

    /**
     * @return the registroBoleto
     */
    public Boleto getRegistroBoleto() {
        return registroBoleto;
    }

    /**
     * @param registroBoleto the registroBoleto to set
     */
    public void setRegistroBoleto(Boleto registroBoleto) {
        this.registroBoleto = registroBoleto;
    }

    /**
     * @return the registroCliente
     */
    public Cliente getRegistroCliente() {
        return registroCliente;
    }

    /**
     * @param registroCliente the registroCliente to set
     */
    public void setRegistroCliente(Cliente registroCliente) {
        this.registroCliente = registroCliente;
    }

    /**
     * @return the registroDetalleAvion
     */
    public DetalleAvion getRegistroDetalleAvion() {
        return registroDetalleAvion;
    }

    /**
     * @param registroDetalleAvion the registroDetalleAvion to set
     */
    public void setRegistroDetalleAvion(DetalleAvion registroDetalleAvion) {
        this.registroDetalleAvion = registroDetalleAvion;
    }

    /**
     * @return the registroDetalleBoleto
     */
    public DetalleBoleto getRegistroDetalleBoleto() {
        return registroDetalleBoleto;
    }

    /**
     * @param registroDetalleBoleto the registroDetalleBoleto to set
     */
    public void setRegistroDetalleBoleto(DetalleBoleto registroDetalleBoleto) {
        this.registroDetalleBoleto = registroDetalleBoleto;
    }

    /**
     * @return the registroDetalleVuelo
     */
    public DetalleVuelo getRegistroDetalleVuelo() {
        return registroDetalleVuelo;
    }

    /**
     * @param registroDetalleVuelo the registroDetalleVuelo to set
     */
    public void setRegistroDetalleVuelo(DetalleVuelo registroDetalleVuelo) {
        this.registroDetalleVuelo = registroDetalleVuelo;
    }

    /**
     * @return the registroMedioContacto
     */
    public MedioContacto getRegistroMedioContacto() {
        return registroMedioContacto;
    }

    /**
     * @param registroMedioContacto the registroMedioContacto to set
     */
    public void setRegistroMedioContacto(MedioContacto registroMedioContacto) {
        this.registroMedioContacto = registroMedioContacto;
    }

    /**
     * @return the registroNodo
     */
    public Nodo getRegistroNodo() {
        return registroNodo;
    }

    /**
     * @param registroNodo the registroNodo to set
     */
    public void setRegistroNodo(Nodo registroNodo) {
        this.registroNodo = registroNodo;
    }

    /**
     * @return the registroPais
     */
    public Pais getRegistroPais() {
        return registroPais;
    }

    /**
     * @param registroPais the registroPais to set
     */
    public void setRegistroPais(Pais registroPais) {
        this.registroPais = registroPais;
    }

    /**
     * @return the registroRuta
     */
    public Ruta getRegistroRuta() {
        return registroRuta;
    }

    /**
     * @param registroRuta the registroRuta to set
     */
    public void setRegistroRuta(Ruta registroRuta) {
        this.registroRuta = registroRuta;
    }

    /**
     * @return the registroTarifa
     */
    public Tarifa getRegistroTarifa() {
        return registroTarifa;
    }

    /**
     * @param registroTarifa the registroTarifa to set
     */
    public void setRegistroTarifa(Tarifa registroTarifa) {
        this.registroTarifa = registroTarifa;
    }

    /**
     * @return the registroTipoAsiento
     */
    public TipoAsiento getRegistroTipoAsiento() {
        return registroTipoAsiento;
    }

    /**
     * @param registroTipoAsiento the registroTipoAsiento to set
     */
    public void setRegistroTipoAsiento(TipoAsiento registroTipoAsiento) {
        this.registroTipoAsiento = registroTipoAsiento;
    }

    /**
     * @return the registroTipoCliente
     */
    public TipoCliente getRegistroTipoCliente() {
        return registroTipoCliente;
    }

    /**
     * @param registroTipoCliente the registroTipoCliente to set
     */
    public void setRegistroTipoCliente(TipoCliente registroTipoCliente) {
        this.registroTipoCliente = registroTipoCliente;
    }

    /**
     * @return the registroTipoMedioContacto
     */
    public TipoMedioContacto getRegistroTipoMedioContacto() {
        return registroTipoMedioContacto;
    }

    /**
     * @param registroTipoMedioContacto the registroTipoMedioContacto to set
     */
    public void setRegistroTipoMedioContacto(TipoMedioContacto registroTipoMedioContacto) {
        this.registroTipoMedioContacto = registroTipoMedioContacto;
    }

    /**
     * @return the registroTipoNodo
     */
    public TipoNodo getRegistroTipoNodo() {
        return registroTipoNodo;
    }

    /**
     * @param registroTipoNodo the registroTipoNodo to set
     */
    public void setRegistroTipoNodo(TipoNodo registroTipoNodo) {
        this.registroTipoNodo = registroTipoNodo;
    }

    /**
     * @return the registroTipoTarifa
     */
    public TipoTarifa getRegistroTipoTarifa() {
        return registroTipoTarifa;
    }

    /**
     * @param registroTipoTarifa the registroTipoTarifa to set
     */
    public void setRegistroTipoTarifa(TipoTarifa registroTipoTarifa) {
        this.registroTipoTarifa = registroTipoTarifa;
    }

    /**
     * @return the registroTipoVuelo
     */
    public TipoVuelo getRegistroTipoVuelo() {
        return registroTipoVuelo;
    }

    /**
     * @param registroTipoVuelo the registroTipoVuelo to set
     */
    public void setRegistroTipoVuelo(TipoVuelo registroTipoVuelo) {
        this.registroTipoVuelo = registroTipoVuelo;
    }

    /**
     * @return the registroVuelo
     */
    public Vuelo getRegistroVuelo() {
        return registroVuelo;
    }

    /**
     * @param registroVuelo the registroVuelo to set
     */
    public void setRegistroVuelo(Vuelo registroVuelo) {
        this.registroVuelo = registroVuelo;
    }

    /**
     * @return the editar
     */
    public boolean isEditar() {
        return editar;
    }

    /**
     * @param editar the editar to set
     */
    public void setEditar(boolean editar) {
        this.editar = editar;
    }

    /**
     * @return the agregar
     */
    public boolean isAgregar() {
        return agregar;
    }

    /**
     * @param agregar the agregar to set
     */
    public void setAgregar(boolean agregar) {
        this.agregar = agregar;
    }
    
}
