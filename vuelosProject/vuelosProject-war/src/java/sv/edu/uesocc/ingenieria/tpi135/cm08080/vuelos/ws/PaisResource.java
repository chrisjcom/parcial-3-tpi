/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.ws;

import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.PaisFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Pais;

/**
 *
 * @author Jhossymar
 */
@RequestScoped
@Path("Pais")
public class PaisResource {
    
    @Inject
    private PaisFacadeLocal pfl;
    
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Pais> findAll() {
        List salida = new ArrayList();
        try {
            if(pfl != null) {
                salida = pfl.findAll();
            }
        } catch (Exception e) {
        }
        return salida;
    }
    
    @GET
    @Path("{nombre}")
    @Produces({MediaType.APPLICATION_JSON})
//    @Consumes se puede decirle al metodo que consumir
    public List<Pais> findByNombre(@PathParam("nombre")String nombre){
        List salida = new ArrayList();
        try {
            if(pfl != null) {
                salida = pfl.findByNombreLike(nombre);
            }
        } catch (Exception e) {
        }
        return salida;
    }
    
    @DELETE
    @Path("{id}")
    public List<Pais> deleteById(@PathParam("id")String id){
        List<Pais> salida = new ArrayList();
        try {
            if(pfl != null) {
                salida = pfl.findByIdPais(id);
                if(!salida.isEmpty()) {
                    for(Pais p : salida) {
                        pfl.remove(p);
                    }
                }
                salida = pfl.findAll();
            }
        } catch (Exception e) {
        }
        return salida;
    }
    
}
