/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.backing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.AvionFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Avion;

/**
 *
 * @author Jhossymar
 */
@ManagedBean
@ViewScoped
public class FrmAviones implements Serializable{
    private LazyDataModel<Avion> modelo;
    @EJB
    private AvionFacadeLocal afl;
    private Avion avion;
    
    public FrmAviones(){}
    
    @PostConstruct
    private void iniciar(){
        
        setModelo(new LazyDataModel<Avion>(){

            @Override
            public List<Avion> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                List salida = new ArrayList();
                if(afl != null){
                    this.setRowCount(afl.count());
                    int[] rango = new int[2];
                    rango[0] = first;
                    rango[1] = pageSize;
                    salida = afl.findRange(rango);
                }
                return salida;
            }

            @Override
            public Object getRowKey(Avion object) {
                return object.getIdAvion();
            }

            @Override
            public Avion getRowData(String rowKey) {
                if(this.getWrappedData()!=null){
                    List<Avion> lista = (List<Avion>) this.getWrappedData();
                    if(!lista.isEmpty()) {
                        for(Avion get : lista) {
                            if(get.getIdAvion().compareTo(Long.parseLong(rowKey))==0) {
                                return get;
                            }
                        }
                    }
                }
                return null;
            }
            
            
            
        });
        
    }
    

    /**
     * @return the modelo
     */
    public LazyDataModel<Avion> getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(LazyDataModel<Avion> modelo) {
        this.modelo = modelo;
    }     

    /**
     * @return the avion
     */
    public Avion getAvion() {
        return avion;
    }

    /**
     * @param avion the avion to set
     */
    public void setAvion(Avion avion) {
        this.avion = avion;
    }
}
