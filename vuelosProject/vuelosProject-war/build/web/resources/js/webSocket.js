/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var wsUri = "ws://localhost:8080/vuelosProject-war/PaisWsocket";
var websocket = new WebSocket(wsUri);

websocket.onopen = function(evt){
  onOpen(evt);
};

websocket.onmessage = function(evt){
  onMessage();
};

function onOpen(){
    console.log("Se establecio conexion con "+wsUri);
}

function onMessage(evt){
    console.log(evt.data);
}

function enviarMensaje(texto){
    websocket.send(texto);
}