/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebamarshal;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.eclipse.persistence.oxm.MediaType;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Pais;

/**
 *
 * @author Jhossymar
 */
public class PruebaMarshal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Pais p = new Pais();
            p.setIdPais("sv");
            p.setNombre("El Salvador");
            JAXBContext jc = JAXBContext.newInstance("sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones");
            Marshaller ma = jc.createMarshaller();
            ma.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
            ma.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT,true);
            ma.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            StringWriter sw = new StringWriter();
            ma.marshal(p, sw);
            System.out.println("JSON Pais: "+sw.toString());
            
            Unmarshaller unmar = jc.createUnmarshaller();
            unmar.setProperty(UnmarshallerProperties.MEDIA_TYPE,MediaType.APPLICATION_JSON);
            unmar.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
            StringReader lector = new StringReader(sw.toString());
            StreamSource json = new StreamSource(lector);
            Pais n = unmar.unmarshal(json,Pais.class).getValue();
            System.out.println("Pais: "+n.getNombre());
            
            JsonReader lector2 = Json.createReader(new StringReader (sw.toString()));
            JsonObject j = lector2.readObject();
            //Si recibieramos varios objetos en el JSON usamos el metodo getJsonArray()
            System.out.println("ID: "+j.getJsonObject("pais").getString("idPais"));
            
        } catch (Exception e) {
            Logger.getLogger(PruebaMarshal.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        
    }
    
}
