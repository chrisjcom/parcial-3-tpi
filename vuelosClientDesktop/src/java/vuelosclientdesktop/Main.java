/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vuelosclientdesktop;

import java.util.List;
import javax.ejb.EJB;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos.PaisFacadeRemote;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Pais;

/**
 *
 * @author Jhossymar
 */
public class Main {
    @EJB
    public static PaisFacadeRemote pfr;
    public static List<Pais> lista;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            lista = pfr.findAll();
            if(!lista.isEmpty()) {
                for(Pais get : lista) {
                    System.out.println(get.getIdPais());
                }
            }
        } catch (Exception e) {
        }
    }
    
}
