/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jhossymar
 */
@Embeddable
public class DetalleBoletoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "codigo_reserva")
    private long codigoReserva;
    @Basic(optional = false)
    @Column(name = "id_vuelo")
    private long idVuelo;

    public DetalleBoletoPK() {
    }

    public DetalleBoletoPK(long codigoReserva, long idVuelo) {
        this.codigoReserva = codigoReserva;
        this.idVuelo = idVuelo;
    }

    public long getCodigoReserva() {
        return codigoReserva;
    }

    public void setCodigoReserva(long codigoReserva) {
        this.codigoReserva = codigoReserva;
    }

    public long getIdVuelo() {
        return idVuelo;
    }

    public void setIdVuelo(long idVuelo) {
        this.idVuelo = idVuelo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) codigoReserva;
        hash += (int) idVuelo;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleBoletoPK)) {
            return false;
        }
        DetalleBoletoPK other = (DetalleBoletoPK) object;
        if (this.codigoReserva != other.codigoReserva) {
            return false;
        }
        if (this.idVuelo != other.idVuelo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.edu.uesocc.ingenieria.tpi135.vuelos.datos.definiciones.DetalleBoletoPK[ codigoReserva=" + codigoReserva + ", idVuelo=" + idVuelo + " ]";
    }
    
}
