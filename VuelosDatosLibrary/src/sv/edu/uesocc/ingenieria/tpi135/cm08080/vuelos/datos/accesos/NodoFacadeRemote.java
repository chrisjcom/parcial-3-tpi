/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Remote;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Nodo;

/**
 *
 * @author Jhossymar
 */
@Remote
public interface NodoFacadeRemote {

    boolean create(Nodo nodo);
    
    Nodo crear(Nodo nodo);

    boolean edit(Nodo nodo);
    
    Nodo editar(Nodo nodo);

    boolean remove(Nodo nodo);

    Nodo find(Object id);

    List<Nodo> findAll();

    List<Nodo> findRange(int[] range);

    int count();
    
}
