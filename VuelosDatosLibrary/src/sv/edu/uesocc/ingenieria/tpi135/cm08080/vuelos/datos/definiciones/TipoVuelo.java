/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jhossymar
 */
@Entity
@Table(name = "tipo_vuelo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoVuelo.findAll", query = "SELECT t FROM TipoVuelo t"),
    @NamedQuery(name = "TipoVuelo.findByIdTipoVuelo", query = "SELECT t FROM TipoVuelo t WHERE t.idTipoVuelo = :idTipoVuelo"),
    @NamedQuery(name = "TipoVuelo.findByNombre", query = "SELECT t FROM TipoVuelo t WHERE t.nombre = :nombre"),
    @NamedQuery(name = "TipoVuelo.findByPublico", query = "SELECT t FROM TipoVuelo t WHERE t.publico = :publico")})
public class TipoVuelo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tipo_vuelo")
    private Integer idTipoVuelo;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "publico")
    private Boolean publico;
    @OneToMany(mappedBy = "idTipoVuelo")
    private List<Vuelo> vueloList;

    public TipoVuelo() {
    }

    public TipoVuelo(Integer idTipoVuelo) {
        this.idTipoVuelo = idTipoVuelo;
    }

    public TipoVuelo(Integer idTipoVuelo, String nombre) {
        this.idTipoVuelo = idTipoVuelo;
        this.nombre = nombre;
    }

    public Integer getIdTipoVuelo() {
        return idTipoVuelo;
    }

    public void setIdTipoVuelo(Integer idTipoVuelo) {
        this.idTipoVuelo = idTipoVuelo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getPublico() {
        return publico;
    }

    public void setPublico(Boolean publico) {
        this.publico = publico;
    }

    @XmlTransient
    public List<Vuelo> getVueloList() {
        return vueloList;
    }

    public void setVueloList(List<Vuelo> vueloList) {
        this.vueloList = vueloList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoVuelo != null ? idTipoVuelo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoVuelo)) {
            return false;
        }
        TipoVuelo other = (TipoVuelo) object;
        if ((this.idTipoVuelo == null && other.idTipoVuelo != null) || (this.idTipoVuelo != null && !this.idTipoVuelo.equals(other.idTipoVuelo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.edu.uesocc.ingenieria.tpi135.vuelos.datos.definiciones.TipoVuelo[ idTipoVuelo=" + idTipoVuelo + " ]";
    }
    
}
