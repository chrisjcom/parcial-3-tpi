/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Remote;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.DetalleBoleto;

/**
 *
 * @author Jhossymar
 */
@Remote
public interface DetalleBoletoFacadeRemote {

    boolean create(DetalleBoleto detalleBoleto);
    
    DetalleBoleto crear(DetalleBoleto detalleBoleto);

    boolean edit(DetalleBoleto detalleBoleto);
    
    DetalleBoleto editar(DetalleBoleto detalleBoleto);

    boolean remove(DetalleBoleto detalleBoleto);

    DetalleBoleto find(Object id);

    List<DetalleBoleto> findAll();

    List<DetalleBoleto> findRange(int[] range);

    int count();
    
}
