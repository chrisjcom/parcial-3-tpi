/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Remote;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Vuelo;

/**
 *
 * @author Jhossymar
 */
@Remote
public interface VueloFacadeRemote {

    boolean create(Vuelo vuelo);
    
    Vuelo crear(Vuelo vuelo);

    boolean edit(Vuelo vuelo);
    
    Vuelo editar(Vuelo vuelo);

    boolean remove(Vuelo vuelo);

    Vuelo find(Object id);

    List<Vuelo> findAll();

    List<Vuelo> findRange(int[] range);

    int count();
    
}
