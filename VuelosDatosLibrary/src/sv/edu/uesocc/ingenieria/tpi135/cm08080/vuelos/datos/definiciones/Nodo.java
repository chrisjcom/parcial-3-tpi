/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jhossymar
 */
@Entity
@Table(name = "nodo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Nodo.findAll", query = "SELECT n FROM Nodo n"),
    @NamedQuery(name = "Nodo.findByIdNodo", query = "SELECT n FROM Nodo n WHERE n.idNodo = :idNodo"),
    @NamedQuery(name = "Nodo.findByNombre", query = "SELECT n FROM Nodo n WHERE n.nombre = :nombre"),
    @NamedQuery(name = "Nodo.findByNombreCorto", query = "SELECT n FROM Nodo n WHERE n.nombreCorto = :nombreCorto"),
    @NamedQuery(name = "Nodo.findByCoordenadasX", query = "SELECT n FROM Nodo n WHERE n.coordenadasX = :coordenadasX"),
    @NamedQuery(name = "Nodo.findByCoordenadasY", query = "SELECT n FROM Nodo n WHERE n.coordenadasY = :coordenadasY")})
public class Nodo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_nodo")
    private Long idNodo;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "nombre_corto")
    private String nombreCorto;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "coordenadas_x")
    private BigDecimal coordenadasX;
    @Column(name = "coordenadas_y")
    private BigDecimal coordenadasY;
    @OneToMany(mappedBy = "idNodoOrigen")
    private List<Ruta> rutaList;
    @OneToMany(mappedBy = "idNodoDestino")
    private List<Ruta> rutaList1;
    @JoinColumn(name = "id_tipo_nodo", referencedColumnName = "id_tipo_nodo")
    @ManyToOne
    private TipoNodo idTipoNodo;

    public Nodo() {
    }

    public Nodo(Long idNodo) {
        this.idNodo = idNodo;
    }

    public Nodo(Long idNodo, String nombre, String nombreCorto) {
        this.idNodo = idNodo;
        this.nombre = nombre;
        this.nombreCorto = nombreCorto;
    }

    public Long getIdNodo() {
        return idNodo;
    }

    public void setIdNodo(Long idNodo) {
        this.idNodo = idNodo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    public BigDecimal getCoordenadasX() {
        return coordenadasX;
    }

    public void setCoordenadasX(BigDecimal coordenadasX) {
        this.coordenadasX = coordenadasX;
    }

    public BigDecimal getCoordenadasY() {
        return coordenadasY;
    }

    public void setCoordenadasY(BigDecimal coordenadasY) {
        this.coordenadasY = coordenadasY;
    }

    @XmlTransient
    public List<Ruta> getRutaList() {
        return rutaList;
    }

    public void setRutaList(List<Ruta> rutaList) {
        this.rutaList = rutaList;
    }

    @XmlTransient
    public List<Ruta> getRutaList1() {
        return rutaList1;
    }

    public void setRutaList1(List<Ruta> rutaList1) {
        this.rutaList1 = rutaList1;
    }

    public TipoNodo getIdTipoNodo() {
        return idTipoNodo;
    }

    public void setIdTipoNodo(TipoNodo idTipoNodo) {
        this.idTipoNodo = idTipoNodo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNodo != null ? idNodo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nodo)) {
            return false;
        }
        Nodo other = (Nodo) object;
        if ((this.idNodo == null && other.idNodo != null) || (this.idNodo != null && !this.idNodo.equals(other.idNodo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.edu.uesocc.ingenieria.tpi135.vuelos.datos.definiciones.Nodo[ idNodo=" + idNodo + " ]";
    }
    
}
