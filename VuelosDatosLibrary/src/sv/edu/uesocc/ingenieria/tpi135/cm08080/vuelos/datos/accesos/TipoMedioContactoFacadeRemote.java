/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Remote;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.TipoMedioContacto;

/**
 *
 * @author Jhossymar
 */
@Remote
public interface TipoMedioContactoFacadeRemote {

    boolean create(TipoMedioContacto tipoMedioContacto);
    
    TipoMedioContacto crear(TipoMedioContacto tipoMedioContacto);

    boolean edit(TipoMedioContacto tipoMedioContacto);
    
    TipoMedioContacto editar(TipoMedioContacto tipoMedioContacto);

    boolean remove(TipoMedioContacto tipoMedioContacto);

    TipoMedioContacto find(Object id);

    List<TipoMedioContacto> findAll();

    List<TipoMedioContacto> findRange(int[] range);

    int count();
    
}
