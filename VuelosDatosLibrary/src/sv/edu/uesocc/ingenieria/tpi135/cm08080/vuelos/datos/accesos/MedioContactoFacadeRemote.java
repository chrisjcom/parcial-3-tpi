/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Remote;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.MedioContacto;

/**
 *
 * @author Jhossymar
 */
@Remote
public interface MedioContactoFacadeRemote {

    boolean create(MedioContacto medioContacto);
    
    MedioContacto crear(MedioContacto medioContacto);

    boolean edit(MedioContacto medioContacto);
    
    MedioContacto editar(MedioContacto medioContacto);

    boolean remove(MedioContacto medioContacto);

    MedioContacto find(Object id);

    List<MedioContacto> findAll();

    List<MedioContacto> findRange(int[] range);

    int count();
    
}
