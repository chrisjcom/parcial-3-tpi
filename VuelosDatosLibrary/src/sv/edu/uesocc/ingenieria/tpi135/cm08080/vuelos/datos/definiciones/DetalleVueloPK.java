/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jhossymar
 */
@Embeddable
public class DetalleVueloPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "id_vuelo")
    private long idVuelo;
    @Basic(optional = false)
    @Column(name = "id_asiento")
    private long idAsiento;

    public DetalleVueloPK() {
    }

    public DetalleVueloPK(long idVuelo, long idAsiento) {
        this.idVuelo = idVuelo;
        this.idAsiento = idAsiento;
    }

    public long getIdVuelo() {
        return idVuelo;
    }

    public void setIdVuelo(long idVuelo) {
        this.idVuelo = idVuelo;
    }

    public long getIdAsiento() {
        return idAsiento;
    }

    public void setIdAsiento(long idAsiento) {
        this.idAsiento = idAsiento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idVuelo;
        hash += (int) idAsiento;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleVueloPK)) {
            return false;
        }
        DetalleVueloPK other = (DetalleVueloPK) object;
        if (this.idVuelo != other.idVuelo) {
            return false;
        }
        if (this.idAsiento != other.idAsiento) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.edu.uesocc.ingenieria.tpi135.vuelos.datos.definiciones.DetalleVueloPK[ idVuelo=" + idVuelo + ", idAsiento=" + idAsiento + " ]";
    }
    
}
