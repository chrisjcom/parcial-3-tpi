/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jhossymar
 */
@Entity
@Table(name = "medio_contacto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MedioContacto.findAll", query = "SELECT m FROM MedioContacto m"),
    @NamedQuery(name = "MedioContacto.findByIdPais", query = "SELECT m FROM MedioContacto m WHERE m.medioContactoPK.idPais = :idPais"),
    @NamedQuery(name = "MedioContacto.findByPasaporte", query = "SELECT m FROM MedioContacto m WHERE m.medioContactoPK.pasaporte = :pasaporte"),
    @NamedQuery(name = "MedioContacto.findByIdTipoMedioContacto", query = "SELECT m FROM MedioContacto m WHERE m.medioContactoPK.idTipoMedioContacto = :idTipoMedioContacto"),
    @NamedQuery(name = "MedioContacto.findByMedio", query = "SELECT m FROM MedioContacto m WHERE m.medio = :medio")})
public class MedioContacto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MedioContactoPK medioContactoPK;
    @Basic(optional = false)
    @Column(name = "medio")
    private String medio;
    @JoinColumn(name = "id_tipo_medio_contacto", referencedColumnName = "id_tipo_medio_contacto", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoMedioContacto tipoMedioContacto;
    @JoinColumns({
        @JoinColumn(name = "id_pais", referencedColumnName = "id_pais", insertable = false, updatable = false),
        @JoinColumn(name = "pasaporte", referencedColumnName = "pasaporte", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Cliente cliente;

    public MedioContacto() {
    }

    public MedioContacto(MedioContactoPK medioContactoPK) {
        this.medioContactoPK = medioContactoPK;
    }

    public MedioContacto(MedioContactoPK medioContactoPK, String medio) {
        this.medioContactoPK = medioContactoPK;
        this.medio = medio;
    }

    public MedioContacto(String idPais, String pasaporte, int idTipoMedioContacto) {
        this.medioContactoPK = new MedioContactoPK(idPais, pasaporte, idTipoMedioContacto);
    }

    public MedioContactoPK getMedioContactoPK() {
        return medioContactoPK;
    }

    public void setMedioContactoPK(MedioContactoPK medioContactoPK) {
        this.medioContactoPK = medioContactoPK;
    }

    public String getMedio() {
        return medio;
    }

    public void setMedio(String medio) {
        this.medio = medio;
    }

    public TipoMedioContacto getTipoMedioContacto() {
        return tipoMedioContacto;
    }

    public void setTipoMedioContacto(TipoMedioContacto tipoMedioContacto) {
        this.tipoMedioContacto = tipoMedioContacto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (medioContactoPK != null ? medioContactoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedioContacto)) {
            return false;
        }
        MedioContacto other = (MedioContacto) object;
        if ((this.medioContactoPK == null && other.medioContactoPK != null) || (this.medioContactoPK != null && !this.medioContactoPK.equals(other.medioContactoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.edu.uesocc.ingenieria.tpi135.vuelos.datos.definiciones.MedioContacto[ medioContactoPK=" + medioContactoPK + " ]";
    }
    
}
