/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jhossymar
 */
@Embeddable
public class MedioContactoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "id_pais")
    private String idPais;
    @Basic(optional = false)
    @Column(name = "pasaporte")
    private String pasaporte;
    @Basic(optional = false)
    @Column(name = "id_tipo_medio_contacto")
    private int idTipoMedioContacto;

    public MedioContactoPK() {
    }

    public MedioContactoPK(String idPais, String pasaporte, int idTipoMedioContacto) {
        this.idPais = idPais;
        this.pasaporte = pasaporte;
        this.idTipoMedioContacto = idTipoMedioContacto;
    }

    public String getIdPais() {
        return idPais;
    }

    public void setIdPais(String idPais) {
        this.idPais = idPais;
    }

    public String getPasaporte() {
        return pasaporte;
    }

    public void setPasaporte(String pasaporte) {
        this.pasaporte = pasaporte;
    }

    public int getIdTipoMedioContacto() {
        return idTipoMedioContacto;
    }

    public void setIdTipoMedioContacto(int idTipoMedioContacto) {
        this.idTipoMedioContacto = idTipoMedioContacto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPais != null ? idPais.hashCode() : 0);
        hash += (pasaporte != null ? pasaporte.hashCode() : 0);
        hash += (int) idTipoMedioContacto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedioContactoPK)) {
            return false;
        }
        MedioContactoPK other = (MedioContactoPK) object;
        if ((this.idPais == null && other.idPais != null) || (this.idPais != null && !this.idPais.equals(other.idPais))) {
            return false;
        }
        if ((this.pasaporte == null && other.pasaporte != null) || (this.pasaporte != null && !this.pasaporte.equals(other.pasaporte))) {
            return false;
        }
        if (this.idTipoMedioContacto != other.idTipoMedioContacto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.edu.uesocc.ingenieria.tpi135.vuelos.datos.definiciones.MedioContactoPK[ idPais=" + idPais + ", pasaporte=" + pasaporte + ", idTipoMedioContacto=" + idTipoMedioContacto + " ]";
    }
    
}
