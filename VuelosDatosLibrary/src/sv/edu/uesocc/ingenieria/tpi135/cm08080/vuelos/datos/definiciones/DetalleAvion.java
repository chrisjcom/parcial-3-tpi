/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jhossymar
 */
@Entity
@Table(name = "detalle_avion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleAvion.findAll", query = "SELECT d FROM DetalleAvion d"),
    @NamedQuery(name = "DetalleAvion.findByIdAsiento", query = "SELECT d FROM DetalleAvion d WHERE d.idAsiento = :idAsiento"),
    @NamedQuery(name = "DetalleAvion.findByHabilitado", query = "SELECT d FROM DetalleAvion d WHERE d.habilitado = :habilitado"),
    @NamedQuery(name = "DetalleAvion.findByVentana", query = "SELECT d FROM DetalleAvion d WHERE d.ventana = :ventana")})
public class DetalleAvion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_asiento")
    private Long idAsiento;
    @Column(name = "habilitado")
    private Boolean habilitado;
    @Column(name = "ventana")
    private Boolean ventana;
    @JoinColumn(name = "id_tipo_asiento", referencedColumnName = "id_tipo_asiento")
    @ManyToOne
    private TipoAsiento idTipoAsiento;
    @JoinColumn(name = "id_avion", referencedColumnName = "id_avion")
    @ManyToOne
    private Avion idAvion;

    public DetalleAvion() {
    }

    public DetalleAvion(Long idAsiento) {
        this.idAsiento = idAsiento;
    }

    public Long getIdAsiento() {
        return idAsiento;
    }

    public void setIdAsiento(Long idAsiento) {
        this.idAsiento = idAsiento;
    }

    public Boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

    public Boolean getVentana() {
        return ventana;
    }

    public void setVentana(Boolean ventana) {
        this.ventana = ventana;
    }

    public TipoAsiento getIdTipoAsiento() {
        return idTipoAsiento;
    }

    public void setIdTipoAsiento(TipoAsiento idTipoAsiento) {
        this.idTipoAsiento = idTipoAsiento;
    }

    public Avion getIdAvion() {
        return idAvion;
    }

    public void setIdAvion(Avion idAvion) {
        this.idAvion = idAvion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAsiento != null ? idAsiento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleAvion)) {
            return false;
        }
        DetalleAvion other = (DetalleAvion) object;
        if ((this.idAsiento == null && other.idAsiento != null) || (this.idAsiento != null && !this.idAsiento.equals(other.idAsiento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.edu.uesocc.ingenieria.tpi135.vuelos.datos.definiciones.DetalleAvion[ idAsiento=" + idAsiento + " ]";
    }
    
}
