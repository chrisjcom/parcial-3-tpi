/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jhossymar
 */
@Entity
@Table(name = "tipo_tarifa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoTarifa.findAll", query = "SELECT t FROM TipoTarifa t"),
    @NamedQuery(name = "TipoTarifa.findByIdTipoTarifa", query = "SELECT t FROM TipoTarifa t WHERE t.idTipoTarifa = :idTipoTarifa"),
    @NamedQuery(name = "TipoTarifa.findByNombre", query = "SELECT t FROM TipoTarifa t WHERE t.nombre = :nombre")})
public class TipoTarifa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tipo_tarifa")
    private Integer idTipoTarifa;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @OneToMany(mappedBy = "idTipoTarifa")
    private List<Tarifa> tarifaList;

    public TipoTarifa() {
    }

    public TipoTarifa(Integer idTipoTarifa) {
        this.idTipoTarifa = idTipoTarifa;
    }

    public TipoTarifa(Integer idTipoTarifa, String nombre) {
        this.idTipoTarifa = idTipoTarifa;
        this.nombre = nombre;
    }

    public Integer getIdTipoTarifa() {
        return idTipoTarifa;
    }

    public void setIdTipoTarifa(Integer idTipoTarifa) {
        this.idTipoTarifa = idTipoTarifa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public List<Tarifa> getTarifaList() {
        return tarifaList;
    }

    public void setTarifaList(List<Tarifa> tarifaList) {
        this.tarifaList = tarifaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoTarifa != null ? idTipoTarifa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoTarifa)) {
            return false;
        }
        TipoTarifa other = (TipoTarifa) object;
        if ((this.idTipoTarifa == null && other.idTipoTarifa != null) || (this.idTipoTarifa != null && !this.idTipoTarifa.equals(other.idTipoTarifa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.edu.uesocc.ingenieria.tpi135.vuelos.datos.definiciones.TipoTarifa[ idTipoTarifa=" + idTipoTarifa + " ]";
    }
    
}
