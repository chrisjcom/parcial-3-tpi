/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jhossymar
 */
@Entity
@Table(name = "tipo_nodo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoNodo.findAll", query = "SELECT t FROM TipoNodo t"),
    @NamedQuery(name = "TipoNodo.findByIdTipoNodo", query = "SELECT t FROM TipoNodo t WHERE t.idTipoNodo = :idTipoNodo"),
    @NamedQuery(name = "TipoNodo.findByNombre", query = "SELECT t FROM TipoNodo t WHERE t.nombre = :nombre")})
public class TipoNodo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tipo_nodo")
    private Integer idTipoNodo;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @OneToMany(mappedBy = "idTipoNodo")
    private List<Nodo> nodoList;

    public TipoNodo() {
    }

    public TipoNodo(Integer idTipoNodo) {
        this.idTipoNodo = idTipoNodo;
    }

    public TipoNodo(Integer idTipoNodo, String nombre) {
        this.idTipoNodo = idTipoNodo;
        this.nombre = nombre;
    }

    public Integer getIdTipoNodo() {
        return idTipoNodo;
    }

    public void setIdTipoNodo(Integer idTipoNodo) {
        this.idTipoNodo = idTipoNodo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public List<Nodo> getNodoList() {
        return nodoList;
    }

    public void setNodoList(List<Nodo> nodoList) {
        this.nodoList = nodoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoNodo != null ? idTipoNodo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoNodo)) {
            return false;
        }
        TipoNodo other = (TipoNodo) object;
        if ((this.idTipoNodo == null && other.idTipoNodo != null) || (this.idTipoNodo != null && !this.idTipoNodo.equals(other.idTipoNodo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.edu.uesocc.ingenieria.tpi135.vuelos.datos.definiciones.TipoNodo[ idTipoNodo=" + idTipoNodo + " ]";
    }
    
}
