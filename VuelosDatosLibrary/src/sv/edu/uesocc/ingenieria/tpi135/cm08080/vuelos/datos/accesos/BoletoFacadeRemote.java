/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Remote;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Boleto;

/**
 *
 * @author Jhossymar
 */
@Remote
public interface BoletoFacadeRemote {

    boolean create(Boleto boleto);
    
    Boleto crear(Boleto boleto);

    boolean edit(Boleto boleto);
    
    Boleto editar(Boleto boleto);

    boolean remove(Boleto boleto);

    Boleto find(Object id);

    List<Boleto> findAll();

    List<Boleto> findRange(int[] range);

    int count();
    
}
