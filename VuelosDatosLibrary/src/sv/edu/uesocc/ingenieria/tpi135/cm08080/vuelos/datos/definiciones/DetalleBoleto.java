/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jhossymar
 */
@Entity
@Table(name = "detalle_boleto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleBoleto.findAll", query = "SELECT d FROM DetalleBoleto d"),
    @NamedQuery(name = "DetalleBoleto.findByCodigoReserva", query = "SELECT d FROM DetalleBoleto d WHERE d.detalleBoletoPK.codigoReserva = :codigoReserva"),
    @NamedQuery(name = "DetalleBoleto.findByIdVuelo", query = "SELECT d FROM DetalleBoleto d WHERE d.detalleBoletoPK.idVuelo = :idVuelo"),
    @NamedQuery(name = "DetalleBoleto.findByIdAsiento", query = "SELECT d FROM DetalleBoleto d WHERE d.idAsiento = :idAsiento"),
    @NamedQuery(name = "DetalleBoleto.findByPrecio", query = "SELECT d FROM DetalleBoleto d WHERE d.precio = :precio"),
    @NamedQuery(name = "DetalleBoleto.findByTipo", query = "SELECT d FROM DetalleBoleto d WHERE d.tipo = :tipo"),
    @NamedQuery(name = "DetalleBoleto.findByFechaConfirmacion", query = "SELECT d FROM DetalleBoleto d WHERE d.fechaConfirmacion = :fechaConfirmacion")})
public class DetalleBoleto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DetalleBoletoPK detalleBoletoPK;
    @Column(name = "id_asiento")
    private BigInteger idAsiento;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "precio")
    private BigDecimal precio;
    @Basic(optional = false)
    @Column(name = "tipo")
    private String tipo;
    @Column(name = "fecha_confirmacion")
    @Temporal(TemporalType.DATE)
    private Date fechaConfirmacion;
    @JoinColumn(name = "id_vuelo", referencedColumnName = "id_vuelo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Vuelo vuelo;
    @JoinColumn(name = "codigo_reserva", referencedColumnName = "codigo_reserva", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Boleto boleto;

    public DetalleBoleto() {
    }

    public DetalleBoleto(DetalleBoletoPK detalleBoletoPK) {
        this.detalleBoletoPK = detalleBoletoPK;
    }

    public DetalleBoleto(DetalleBoletoPK detalleBoletoPK, String tipo) {
        this.detalleBoletoPK = detalleBoletoPK;
        this.tipo = tipo;
    }

    public DetalleBoleto(long codigoReserva, long idVuelo) {
        this.detalleBoletoPK = new DetalleBoletoPK(codigoReserva, idVuelo);
    }

    public DetalleBoletoPK getDetalleBoletoPK() {
        return detalleBoletoPK;
    }

    public void setDetalleBoletoPK(DetalleBoletoPK detalleBoletoPK) {
        this.detalleBoletoPK = detalleBoletoPK;
    }

    public BigInteger getIdAsiento() {
        return idAsiento;
    }

    public void setIdAsiento(BigInteger idAsiento) {
        this.idAsiento = idAsiento;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getFechaConfirmacion() {
        return fechaConfirmacion;
    }

    public void setFechaConfirmacion(Date fechaConfirmacion) {
        this.fechaConfirmacion = fechaConfirmacion;
    }

    public Vuelo getVuelo() {
        return vuelo;
    }

    public void setVuelo(Vuelo vuelo) {
        this.vuelo = vuelo;
    }

    public Boleto getBoleto() {
        return boleto;
    }

    public void setBoleto(Boleto boleto) {
        this.boleto = boleto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detalleBoletoPK != null ? detalleBoletoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleBoleto)) {
            return false;
        }
        DetalleBoleto other = (DetalleBoleto) object;
        if ((this.detalleBoletoPK == null && other.detalleBoletoPK != null) || (this.detalleBoletoPK != null && !this.detalleBoletoPK.equals(other.detalleBoletoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.edu.uesocc.ingenieria.tpi135.vuelos.datos.definiciones.DetalleBoleto[ detalleBoletoPK=" + detalleBoletoPK + " ]";
    }
    
}
