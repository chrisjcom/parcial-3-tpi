/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jhossymar
 */
@Entity
@Table(name = "ruta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ruta.findAll", query = "SELECT r FROM Ruta r"),
    @NamedQuery(name = "Ruta.findByIdRuta", query = "SELECT r FROM Ruta r WHERE r.idRuta = :idRuta"),
    @NamedQuery(name = "Ruta.findByHoraSalida", query = "SELECT r FROM Ruta r WHERE r.horaSalida = :horaSalida"),
    @NamedQuery(name = "Ruta.findByHoraLlegada", query = "SELECT r FROM Ruta r WHERE r.horaLlegada = :horaLlegada"),
    @NamedQuery(name = "Ruta.findByDiaSalida", query = "SELECT r FROM Ruta r WHERE r.diaSalida = :diaSalida"),
    @NamedQuery(name = "Ruta.findByDiaLlegada", query = "SELECT r FROM Ruta r WHERE r.diaLlegada = :diaLlegada")})
public class Ruta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_ruta")
    private Long idRuta;
    @Column(name = "hora_salida")
    @Temporal(TemporalType.TIME)
    private Date horaSalida;
    @Column(name = "hora_llegada")
    @Temporal(TemporalType.TIME)
    private Date horaLlegada;
    @Column(name = "dia_salida")
    private Integer diaSalida;
    @Column(name = "dia_llegada")
    private Integer diaLlegada;
    @JoinColumn(name = "id_nodo_origen", referencedColumnName = "id_nodo")
    @ManyToOne
    private Nodo idNodoOrigen;
    @JoinColumn(name = "id_nodo_destino", referencedColumnName = "id_nodo")
    @ManyToOne
    private Nodo idNodoDestino;
    @JoinColumn(name = "id_avion", referencedColumnName = "id_avion")
    @ManyToOne
    private Avion idAvion;
    @OneToMany(mappedBy = "idRuta")
    private List<Vuelo> vueloList;

    public Ruta() {
    }

    public Ruta(Long idRuta) {
        this.idRuta = idRuta;
    }

    public Long getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(Long idRuta) {
        this.idRuta = idRuta;
    }

    public Date getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(Date horaSalida) {
        this.horaSalida = horaSalida;
    }

    public Date getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(Date horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    public Integer getDiaSalida() {
        return diaSalida;
    }

    public void setDiaSalida(Integer diaSalida) {
        this.diaSalida = diaSalida;
    }

    public Integer getDiaLlegada() {
        return diaLlegada;
    }

    public void setDiaLlegada(Integer diaLlegada) {
        this.diaLlegada = diaLlegada;
    }

    public Nodo getIdNodoOrigen() {
        return idNodoOrigen;
    }

    public void setIdNodoOrigen(Nodo idNodoOrigen) {
        this.idNodoOrigen = idNodoOrigen;
    }

    public Nodo getIdNodoDestino() {
        return idNodoDestino;
    }

    public void setIdNodoDestino(Nodo idNodoDestino) {
        this.idNodoDestino = idNodoDestino;
    }

    public Avion getIdAvion() {
        return idAvion;
    }

    public void setIdAvion(Avion idAvion) {
        this.idAvion = idAvion;
    }

    @XmlTransient
    public List<Vuelo> getVueloList() {
        return vueloList;
    }

    public void setVueloList(List<Vuelo> vueloList) {
        this.vueloList = vueloList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRuta != null ? idRuta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ruta)) {
            return false;
        }
        Ruta other = (Ruta) object;
        if ((this.idRuta == null && other.idRuta != null) || (this.idRuta != null && !this.idRuta.equals(other.idRuta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.edu.uesocc.ingenieria.tpi135.vuelos.datos.definiciones.Ruta[ idRuta=" + idRuta + " ]";
    }
    
}
