/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jhossymar
 */
@Entity
@Table(name = "detalle_vuelo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleVuelo.findAll", query = "SELECT d FROM DetalleVuelo d"),
    @NamedQuery(name = "DetalleVuelo.findByIdVuelo", query = "SELECT d FROM DetalleVuelo d WHERE d.detalleVueloPK.idVuelo = :idVuelo"),
    @NamedQuery(name = "DetalleVuelo.findByIdAsiento", query = "SELECT d FROM DetalleVuelo d WHERE d.detalleVueloPK.idAsiento = :idAsiento"),
    @NamedQuery(name = "DetalleVuelo.findByPrecio", query = "SELECT d FROM DetalleVuelo d WHERE d.precio = :precio"),
    @NamedQuery(name = "DetalleVuelo.findByObservaciones", query = "SELECT d FROM DetalleVuelo d WHERE d.observaciones = :observaciones")})
public class DetalleVuelo implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DetalleVueloPK detalleVueloPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "precio")
    private BigDecimal precio;
    @Column(name = "observaciones")
    private String observaciones;
    @JoinColumn(name = "id_vuelo", referencedColumnName = "id_vuelo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Vuelo vuelo;

    public DetalleVuelo() {
    }

    public DetalleVuelo(DetalleVueloPK detalleVueloPK) {
        this.detalleVueloPK = detalleVueloPK;
    }

    public DetalleVuelo(long idVuelo, long idAsiento) {
        this.detalleVueloPK = new DetalleVueloPK(idVuelo, idAsiento);
    }

    public DetalleVueloPK getDetalleVueloPK() {
        return detalleVueloPK;
    }

    public void setDetalleVueloPK(DetalleVueloPK detalleVueloPK) {
        this.detalleVueloPK = detalleVueloPK;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Vuelo getVuelo() {
        return vuelo;
    }

    public void setVuelo(Vuelo vuelo) {
        this.vuelo = vuelo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detalleVueloPK != null ? detalleVueloPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleVuelo)) {
            return false;
        }
        DetalleVuelo other = (DetalleVuelo) object;
        if ((this.detalleVueloPK == null && other.detalleVueloPK != null) || (this.detalleVueloPK != null && !this.detalleVueloPK.equals(other.detalleVueloPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.edu.uesocc.ingenieria.tpi135.vuelos.datos.definiciones.DetalleVuelo[ detalleVueloPK=" + detalleVueloPK + " ]";
    }
    
}
