/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Remote;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.Tarifa;

/**
 *
 * @author Jhossymar
 */
@Remote
public interface TarifaFacadeRemote {

    boolean create(Tarifa tarifa);
    
    Tarifa crear(Tarifa tarifa);

    boolean edit(Tarifa tarifa);
    
    Tarifa editar(Tarifa tarifa);

    boolean remove(Tarifa tarifa);

    Tarifa find(Object id);

    List<Tarifa> findAll();

    List<Tarifa> findRange(int[] range);

    int count();
    
}
