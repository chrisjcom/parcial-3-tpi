/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jhossymar
 */
@Entity
@Table(name = "vuelo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vuelo.findAll", query = "SELECT v FROM Vuelo v"),
    @NamedQuery(name = "Vuelo.findByIdVuelo", query = "SELECT v FROM Vuelo v WHERE v.idVuelo = :idVuelo"),
    @NamedQuery(name = "Vuelo.findByObservaciones", query = "SELECT v FROM Vuelo v WHERE v.observaciones = :observaciones"),
    @NamedQuery(name = "Vuelo.findByFechaSalida", query = "SELECT v FROM Vuelo v WHERE v.fechaSalida = :fechaSalida"),
    @NamedQuery(name = "Vuelo.findByFechaLlegada", query = "SELECT v FROM Vuelo v WHERE v.fechaLlegada = :fechaLlegada")})
public class Vuelo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_vuelo")
    private Long idVuelo;
    @Column(name = "observaciones")
    private String observaciones;
    @Basic(optional = false)
    @Column(name = "fecha_salida")
    @Temporal(TemporalType.DATE)
    private Date fechaSalida;
    @Basic(optional = false)
    @Column(name = "fecha_llegada")
    @Temporal(TemporalType.DATE)
    private Date fechaLlegada;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vuelo")
    private List<DetalleBoleto> detalleBoletoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vuelo")
    private List<DetalleVuelo> detalleVueloList;
    @JoinColumn(name = "id_tipo_vuelo", referencedColumnName = "id_tipo_vuelo")
    @ManyToOne
    private TipoVuelo idTipoVuelo;
    @JoinColumn(name = "id_tarifa", referencedColumnName = "id_tarifa")
    @ManyToOne
    private Tarifa idTarifa;
    @JoinColumn(name = "id_ruta", referencedColumnName = "id_ruta")
    @ManyToOne
    private Ruta idRuta;

    public Vuelo() {
    }

    public Vuelo(Long idVuelo) {
        this.idVuelo = idVuelo;
    }

    public Vuelo(Long idVuelo, Date fechaSalida, Date fechaLlegada) {
        this.idVuelo = idVuelo;
        this.fechaSalida = fechaSalida;
        this.fechaLlegada = fechaLlegada;
    }

    public Long getIdVuelo() {
        return idVuelo;
    }

    public void setIdVuelo(Long idVuelo) {
        this.idVuelo = idVuelo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public Date getFechaLlegada() {
        return fechaLlegada;
    }

    public void setFechaLlegada(Date fechaLlegada) {
        this.fechaLlegada = fechaLlegada;
    }

    @XmlTransient
    public List<DetalleBoleto> getDetalleBoletoList() {
        return detalleBoletoList;
    }

    public void setDetalleBoletoList(List<DetalleBoleto> detalleBoletoList) {
        this.detalleBoletoList = detalleBoletoList;
    }

    @XmlTransient
    public List<DetalleVuelo> getDetalleVueloList() {
        return detalleVueloList;
    }

    public void setDetalleVueloList(List<DetalleVuelo> detalleVueloList) {
        this.detalleVueloList = detalleVueloList;
    }

    public TipoVuelo getIdTipoVuelo() {
        return idTipoVuelo;
    }

    public void setIdTipoVuelo(TipoVuelo idTipoVuelo) {
        this.idTipoVuelo = idTipoVuelo;
    }

    public Tarifa getIdTarifa() {
        return idTarifa;
    }

    public void setIdTarifa(Tarifa idTarifa) {
        this.idTarifa = idTarifa;
    }

    public Ruta getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(Ruta idRuta) {
        this.idRuta = idRuta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVuelo != null ? idVuelo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vuelo)) {
            return false;
        }
        Vuelo other = (Vuelo) object;
        if ((this.idVuelo == null && other.idVuelo != null) || (this.idVuelo != null && !this.idVuelo.equals(other.idVuelo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.edu.uesocc.ingenieria.tpi135.vuelos.datos.definiciones.Vuelo[ idVuelo=" + idVuelo + " ]";
    }
    
}
