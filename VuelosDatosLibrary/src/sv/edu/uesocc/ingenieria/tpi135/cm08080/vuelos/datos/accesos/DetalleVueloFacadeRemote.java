/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.accesos;

import java.util.List;
import javax.ejb.Remote;
import sv.edu.uesocc.ingenieria.tpi135.cm08080.vuelos.datos.definiciones.DetalleVuelo;

/**
 *
 * @author Jhossymar
 */
@Remote
public interface DetalleVueloFacadeRemote {

    boolean create(DetalleVuelo detalleVuelo);
    
    DetalleVuelo crear(DetalleVuelo detalleVuelo);

    boolean edit(DetalleVuelo detalleVuelo);
    
    DetalleVuelo editar(DetalleVuelo detalleVuelo);

    boolean remove(DetalleVuelo detalleVuelo);

    DetalleVuelo find(Object id);

    List<DetalleVuelo> findAll();

    List<DetalleVuelo> findRange(int[] range);

    int count();
    
}
